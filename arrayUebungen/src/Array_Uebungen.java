import java.util.Scanner;

public class Array_Uebungen {

    public static void main(String[] args) {
        // Einfache Uebungen zu Arrays
        System.out.println("Einfache Uebungen zu Arrays");
        // Aufgabe 3
        System.out.println("Aufgabe 3");
        // Eingabe Scanner
        Scanner input = new Scanner(System.in);

        // Array mit Zeihen
        char[] stringInputAsArray = new char[5];

        // Eingabe der Zeichen
        System.out.println("Geben Sie fuenf Zeichen ein:");
        String stringInput = input.nextLine();

        // Zeichen Array wird mit Eingaben gefuellt
        for (int i = 0; i < 5; i++){
            stringInputAsArray[i] = stringInput.charAt(i);
        }

        // Umgekehrte Ausgabe des Arrays
        System.out.println("Umgekehrte Ausgabe des Arrays");
        for(int i = 4; i >= 0; i--){
            System.out.println(stringInputAsArray[i]);
        }
        System.out.println();


        // Aufgabe 4
        System.out.println("Aufgabe 4");
        int[] lottozahlen = {3, 7, 12, 18, 37, 42};

        // a
        // Ausgabe des Arrays
        System.out.print('[');
        for(int i = 0; i < lottozahlen.length; i++){
            System.out.printf(" %d ", lottozahlen[i]);
        }
        System.out.println(']');

        //b
        System.out.println("Pruefung ob die Zahl 12 im Array ist:");
        istEnthalten(12, lottozahlen);

        System.out.println("Pruefung ob die Zahl 13 im Array ist:");
        istEnthalten(13, lottozahlen);

    }

    public static void istEnthalten(int needle, int[] haystack ){
        for(int i = 0; i < haystack.length; i++){
            if(haystack[i] == needle){
                System.out.printf("Die Zahl %d ist in der Ziehung enthalten.%n", needle);
                return;
            } else {
                System.out.printf("Die Zahl %d ist in der nicht Ziehung enthalten%n.", needle);
                return;
            }
        }
    }
}
