package einfuerungMethoden_A3_1;


public class Mittelwert {

   public static void main(String[] args) {

      // (E) "Eingabe"
      // Werte für x und y festlegen:
      // ===========================
      double x = 2.0;
      double y = 4.0;
      double m;
      
      // (V) Verarbeitung
      // Mittelwert von x und y berechnen: 
      // ================================
      m = (x + y) / 2.0;
      
      // (A) Ausgabe
      // Ergebnis auf der Konsole ausgeben:
      // =================================
      System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, m);
      
      // A3.1: Einführung Methoden
      System.out.println("\n\nBerechnung des Mittelwertes mit der Methode 'berechneMittelwert()'");
      berechneMittelwert(x,y);
      
   }
   
   public static void berechneMittelwert(double x,  double y) {
	   double m = (x + y) / 2.0;
	   
	   System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f", x, y, m);
   }
}
