package einfuerungMethoden_A3_1;

public class Quadrieren {
	   
	public static void main(String[] args) {

		// (E) "Eingabe"
		// Wert f¸r x festlegen:
		// ===========================
		System.out.println("Dieses Programm berechnet die Quadratzahl x≤");
		System.out.println("---------------------------------------------");
		double x = 5;
		// Berechnung des Quadrats von x 
		double ergebnis = quadriere(x);
				
		// (V) Verarbeitung
		// Mittelwert von x und y berechnen:
		// ================================
		

		// (A) Ausgabe
		// Ergebnis auf der Konsole ausgeben:
		// =================================
		System.out.printf("x = %.2f und x≤= %.2f\n", x, ergebnis);
	}
	
	static double quadriere(double x) {
		return x * x;
	}
}
