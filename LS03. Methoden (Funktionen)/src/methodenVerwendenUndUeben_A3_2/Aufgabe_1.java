package methodenVerwendenUndUeben_A3_2;

public class Aufgabe_1 {
	
	public static void main(String[] args) {
		// Berechnung des Mittelwertes mit der Ausgelagerten Methode 'berechneMittelwert()'
		
		// Aufgabe1 von AB-MethodenProgrammieruebungen
		
		// Parameter fuer die Methode berechneMittelwert()
	    double x = 2.0;
	    double y = 4.0;
	    
	    // Auslagerung der Methode berechneMittelwert() in die Variable 'm'
	    double m = berechneMittelwert(x,y);
	    
	    
	    // Ausgabe der ausgelagerten Methode berechneMittelwert() in der Variable 'm'
	    gebeMittelwertAus(x, y, m); 
	}
	
	// Implementierun von berechneMittelwert()
	public static double berechneMittelwert(double x,  double y) {
		double m = (x + y) / 2.0;
		return m;
	}
	
	// Implementierun von gebeMittelwertAus
	public static void gebeMittelwertAus(double x, double y, double m) {
		System.out.printf("Der Mittelwert von %f und %f ist %f", x, y, m);
	}
}

