package methodenVerwendenUndUeben_A3_2;

import java.util.Scanner;

public class Aufgabe2 {
	
	public static Scanner myScanner = new Scanner(System.in);

	public static void main(String[] args) {
			// Benutzereingaben lesen mit leseEingabe
			
			// leseArtikelEin
			String artikel = leseArtikel();

			// leseAnzahlEin
			int anzahl = leseAnzahlEin();

			// leseNettoPreisEin und leseMwstEin
			// beide nutzen die gleiche Methode, da sie den selben Datentyp nutzen
			System.out.println("Geben Sie den Nettopreis ein:");
			double preis = leseNetPreisOderMwstEin();

			System.out.println("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
			double mwst = leseNetPreisOderMwstEin();

			// Verarbeiten
			double nettogesamtpreis = berechneNetGesamtPreis(anzahl, preis);
			double bruttogesamtpreis = berechneBrutGesamtPreis(nettogesamtpreis, mwst);

			// Ausgeben
			druckeRechnung(artikel, anzahl, nettogesamtpreis, bruttogesamtpreis, mwst);
	
	}
	
	// Eingabe Methoden
	
		// Implementierun von leseArtikel 
		static String leseArtikel() {
			return myScanner.next();
		}
		
		// Implementierun von leseArtikel 
		static int leseAnzahlEin() {
			System.out.println("Geben Sie die Anzahl ein:");
			return myScanner.nextInt();
		}
		
		// // leseNettoPreisEin und leseMwstEin
		static double leseNetPreisOderMwstEin() {
			return myScanner.nextDouble();
		}
		
	// Verarbeitungsmethoden
	
		static double berechneNetGesamtPreis(int anzahl, double preis) {
			return anzahl * preis;
		}
		
		static double berechneBrutGesamtPreis(double nettogesamtpreis, double mwst) {
			return nettogesamtpreis * (1 + mwst / 100);
		}
		
	// Ausgabemethoden
		static void druckeRechnung(String artikel,int anzahl, double nettogesamtpreis, double bruttogesamtpreis, double mwst) {
			System.out.println("\tRechnung");
			System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
			System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
		}
		
}
