import java.util.Scanner;

class FahrkartenautomatMitMethoden
{
    public static void main(String[] args)
    {
    	Scanner inScanner = new Scanner(System.in);
    	boolean weitereFahrkahrten = true;
    	while (true) {
        	// Fahrkartenanzahl und Betrag werden erfasst
        	double zuZahlenderBetrag = fahrkartenbestellungErfassen();
        	
        	// Fahrkarten werden bezahlt und der Rueckbetrag gespeichert
        	double rueckbetrag = fahrkartenBezahlen(zuZahlenderBetrag);
        	
        	// Fahrten werden ausgegeben 
        	fahrkartenAusgeben();
        	
        	// rueckbetrag wird berechnet, falls noetig und ausgegeben
        	if(rueckbetrag > 0.0) {
        		rueckgeldAusgeben(rueckbetrag);
        	}
        	
        	// Abschied des Automaten
        	abschied();

        	System.out.println("\n---------------------------------------");
        	System.out.println("Moechten Sie weitere Fahrkaten kaufen?");
        	String entscheidung = inScanner.next();
        	weitereFahrkahrten = entscheidung.equals("nein") || entscheidung.equals("n") ? false : true; 
		}

    }
    
    static boolean isValidEingabe(double input) {
    	return input > 0 && input <= 10;
    }
    
    st
    
    static double fahrkartenbestellungErfassen() {
    	Scanner tastatur = new Scanner(System.in);
    	double zuZahlenderBetrag; 
    	double anzahlFahrkaten;
    	String fehlerMeldungAnzahl = "1 Ticket wird Festgelegt.\nDie Ticketanzahl muss zwischen 1 und 10 sein!";
    	String fehlerMeldungBetrag = "Der Ticketpreis muss positiv sein.";

        // Anzahl der Fahrkarten
        System.out.print("Wie viele Fahrkarten moechten Sie kaufen: ");
        anzahlFahrkaten = tastatur.nextDouble();
        
        if (!isValidEingabe(anzahlFahrkaten)) {
        	anzahlFahrkaten = 1;
        	System.out.println(fehlerMeldungAnzahl);
        }
        
        System.out.print("Zu zahlender Betrag (EURO): ");
        zuZahlenderBetrag = tastatur.nextDouble();
        if (!isValidEingabe(zuZahlenderBetrag)) {
			zuZahlenderBetrag = 1;
			System.out.println(fehlerMeldungBetrag);
		}
        
        // zu Zahlender Betrag wird fuer die Anzahl der Fahkarten angepasst
        return zuZahlenderBetrag *= anzahlFahrkaten;
    }
    
    static double fahrkartenBezahlen(double zuZahlen) {
    	Scanner tastatur = new Scanner(System.in);
    	double eingezahlterGesamtbetrag;
    	double eingeworfeneMuenze;
    	double rueckgabebetrag;
    	
        // Geldeinwurf
        // -----------
        eingezahlterGesamtbetrag = 0.0;
        while(eingezahlterGesamtbetrag < zuZahlen)
        {
     	// ergaentzt zwei die Nachkommastellen
     	   System.out.printf("Noch zu zahlen: %.2f " ,  (zuZahlen - eingezahlterGesamtbetrag)); 
     	   System.out.print("Eingabe (mind. 5Ct, hoechstens 2 Euro): ");
     	   eingeworfeneMuenze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneMuenze;
        }
        rueckgabebetrag = eingezahlterGesamtbetrag - zuZahlen;
        return rueckgabebetrag;
    }
    
    static void fahrkartenAusgeben() {

        // Fahrscheinausgabe
        // -----------------
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           warte(250);
        }
        System.out.println("\n\n");
    }
    
    static void warte(int millisekunden) {
    	try {
 			Thread.sleep(millisekunden);   
 		} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
    }
    
    static void rueckgeldAusgeben(double rueckgabebetrag) {
    	final String EURO = "€ ";
    	final String CENT = "ct";
        // Rueckgeldberechnung und -Ausgabe
        // -------------------------------
        
        {
     	   System.out.println("Der Rueckgabebetrag in Hoehe von " + rueckgabebetrag + " EURO");
     	   System.out.println("wird in folgenden Muenzen ausgezahlt:");

            while(rueckgabebetrag >= 2.0) // 2 EURO-Muenzen
            {
              muenzeAusgeben(2.0, EURO);
 	          rueckgabebetrag -= 2.0;
            }
            while(rueckgabebetrag >= 1.0) // 1 EURO-Muenzen
            {
              muenzeAusgeben(1.0, EURO);
 	          rueckgabebetrag -= 1.0;
            }
            while(rueckgabebetrag >= 0.5) // 50 CENT-Muenzen
            {
              muenzeAusgeben(0.5, CENT);;
 	          rueckgabebetrag -= 0.5;
            }
            while(rueckgabebetrag >= 0.2) // 20 CENT-Muenzen
            {
              muenzeAusgeben(0.2, CENT);;
  	          rueckgabebetrag -= 0.2;
            }
            while(rueckgabebetrag >= 0.1) // 10 CENT-Muenzen
            {
              muenzeAusgeben(0.1, CENT);
 	          rueckgabebetrag -= 0.1;
            }
            while(rueckgabebetrag >= 0.05)// 5 CENT-Muenzen
            {
              muenzeAusgeben(0.05, CENT);
  	          rueckgabebetrag -= 0.05;
            }
        }    
    }
    
    static void muenzeAusgeben(double betrag, String einheit) {
    	betrag = betrag < 1.0 ? betrag * 100 : betrag;
    	System.out.printf("%d %s", (int) betrag, einheit);
    }
    
    static void abschied() {
    	System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                "vor Fahrtantritt entwerten zu lassen!\n"+
                "Wir wuenschen Ihnen eine gute Fahrt.");
    }
    
}