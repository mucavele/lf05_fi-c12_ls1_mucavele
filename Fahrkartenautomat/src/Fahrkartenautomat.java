﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
       double zuZahlenderBetrag; 
       double eingezahlterGesamtbetrag;
       double eingeworfeneMünze;
       double rückgabebetrag;
       
       /* 
	       Im Quelltext sind alle Variable doubles, die Variablen sind:
	       	zuZahlenderBetrag wird zugewiesen, multipliziert, verglichen und subtrahiert
	       	
	       	eingezahlterGesamtbetrag wird zugewiesen, verglichen und subtrahiert
	       	
	       	eingeworfeneMünze wird zugewiesen, addiert
	       	
	       	rückgabebetrag wird zugewiesen, in if und while Anweeisungen verglichen und dementsprechend subtrahiert
	       	
	       	anzahlFahrkaten wird zugewiesen und multipliziert 
        */
       	
       
       // fuer die Anzahl der Fahkarten wird der double Datentyp genutzt,
       // weil ein anderer Datentyp eine implitzite Typumwandlug zu double erzwingen wuerde.
       double anzahlFahrkaten;

       // Anzahl der Fahrkarten
       System.out.print("Wie vieele Fahrkarten möchten Sie kaufen: ");
       anzahlFahrkaten = tastatur.nextDouble();
       
       System.out.print("Zu zahlender Betrag (EURO): ");
       zuZahlenderBetrag = tastatur.nextDouble();
       
       // zu Zahlender Betrag wird fuer die Anzahl der Fahkarten angepasst
       zuZahlenderBetrag *= anzahlFahrkaten;
       

       // Geldeinwurf
       // -----------
       eingezahlterGesamtbetrag = 0.0;
       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
       {
    	// ergaentzt zwei die Nachkommastellen
    	   System.out.printf("Noch zu zahlen: %.2f " ,  (zuZahlenderBetrag - eingezahlterGesamtbetrag)); 
    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
    	   eingeworfeneMünze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneMünze;
       }

       // Fahrscheinausgabe
       // -----------------
       System.out.println("\nFahrschein wird ausgegeben");
       for (int i = 0; i < 8; i++)
       {
          System.out.print("=");
          try {
			Thread.sleep(250);   
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       }
       System.out.println("\n\n");

       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
       rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
       if(rückgabebetrag > 0.0)
       {
    	   System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
           {
        	  System.out.println("2 EURO");
	          rückgabebetrag -= 2.0;
           }
           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
           {
        	  System.out.println("1 EURO");
	          rückgabebetrag -= 1.0;
           }
           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
           {
        	  System.out.println("50 CENT");
	          rückgabebetrag -= 0.5;
           }
           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
           {
        	  System.out.println("20 CENT");
 	          rückgabebetrag -= 0.2;
           }
           while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
           {
        	  System.out.println("10 CENT");
	          rückgabebetrag -= 0.1;
           }
           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
           {
        	  System.out.println("5 CENT");
 	          rückgabebetrag -= 0.05;
           }
       }

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.");
       
       
    }
}