package fahrkartenautomat;

import java.util.Scanner;

public class Fahrkartenautomat {
    // main
    public static void main(String[] args)
    {
        Scanner inScanner = new Scanner(System.in);
        // kontrolliert die endlosschleife
        boolean weitereFahrkarten = true;
        while (weitereFahrkarten) {
            // Fahrkartenanzahl und Betrag werden erfasst
            double zuZahlenderBetrag = fahrkartenbestellungErfassen();

            // Fahrkarten werden bezahlt und der Rueckbetrag gespeichert
            double rueckbetrag = fahrkartenBezahlen(zuZahlenderBetrag);

            // Fahrten werden ausgegeben
            fahrkartenAusgeben();

            // rueckbetrag wird berechnet, falls noetig und ausgegeben
            if(rueckbetrag > 0.0) {
                rueckgeldAusgeben(rueckbetrag);
            }

            System.out.println("\n---------------------------------------");
            System.out.println("Moechten Sie weitere Fahrkarten kaufen?");
            String entscheidung = inScanner.next();
            weitereFahrkarten = !entscheidung.equals("nein") && !entscheidung.equals("n");
        }

        // Abschied des Automaten
        abschied();
    }

    // Nutzer kann aus einer Auswahl von Fahrkarten waehlen
    static double fahrkartenartWaehlen(){
        Scanner kartenWahl = new Scanner(System.in);
        /*
        Durch die Implementierung der Arrays ist das Hinzufügen von neuen Angeboten
        Flexibler geworden. Vorher musste jedes einzelne Angebot in einem eigenen
        Ausgabebefehl kodiert werden und jetzt geschieht diese Ausgabe für alle Angebote
        in einer Schleife.

        Falls man bsw. ein Anschluss Fahrschein hinzufügen möchte,
        geschieht das über die beiden Arrays.
        Erst gibt man den Namen des neuen Fahrscheins in den Array "fahrkartenArten"
        und anschließend kommt der dazugehörige Preis in den "fahrkartenPreise" Arrays.
         */

        // Angebot der Fahrkarten,
        // die index Positionen (+ 1) dienen als Auswahlnummer
        String[] fahrkartenArten = {
                "Einzelfahrschein Berlin AB",
                "Einzelfahrschein Berlin BC",
                "Einzelfahrschein Berlin ABC",
                "Kurzstrecke",
                "Tageskarte Berlin AB",
                "Tageskarte Berlin BC",
                "Tageskarte Berlin ABC",
                "Kleingruppen-Tageskarte Berlin AB",
                "Kleingruppen-Tageskarte Berlin BC",
                "Kleingruppen-Tageskarte Berlin ABC"
        };

        // Preise der Fahrkarten stehen in beziehung mit den Fahrkarten Angebot
        double[] fahrkartenPreise = {
                2.90, 3.30, 3.60, 1.90, 8.60,
                9.00, 9.60, 23.50, 24.30, 24.90
        };
        double preis;
        int wahl;

        // Ausgabe der Fahrkartenangebote
        System.out.println("Waehlen Sie ihre Wunschfahrkarte fuer Berlin aus:");
        for(int i = 0; i < fahrkartenArten.length; i++){
            System.out.printf("%s [%.2f EUR] (%d)%n",
                    fahrkartenArten[i], fahrkartenPreise[i], i+1);
        }

        wahl = kartenWahl.nextInt();
        while(wahl > 10 || wahl < 1){
            System.out.println(">>Falsche Eingabe<<");
            wahl = kartenWahl.nextInt();
        }

        System.out.println("Ihre Wahl: " + wahl);
        preis = fahrkartenPreise[wahl-1];
        return preis;
    }

    // Fahrkartenerfassung
    static double fahrkartenbestellungErfassen() {
        Scanner tastatur = new Scanner(System.in);
        double zuZahlenderBetrag = 0;
        boolean bezahlen = true;
        int anzahlFahrkarten = 0;
        char bezahlenAntwort;

        while(bezahlen){
            zuZahlenderBetrag += fahrkartenartWaehlen();
            System.out.printf("Zwischensumme: %.2f%n", zuZahlenderBetrag);
            ++anzahlFahrkarten;
            System.out.print("""
                                Möchten Sie weitere Fahrkarten hinzufügen?
                                j = ja oder n = nein
                                """);
            bezahlenAntwort = tastatur.nextLine().charAt(0);
            if(bezahlenAntwort == 'n' || bezahlenAntwort == 'N'){
                bezahlen = false;
            }
        }

        System.out.println("Anzahl der Tickets: " + anzahlFahrkarten);

        // zu Zahlender Betrag wird fuer die Anzahl der Fahkarten angepasst
        return zuZahlenderBetrag;
    }

    // Fahrkarten Bezahlung
    static double fahrkartenBezahlen(double zuZahlen) {
        Scanner tastatur = new Scanner(System.in);
        double eingezahlterGesamtbetrag;
        double eingeworfeneMuenze;
        double rueckgabebetrag;

        // Geldeinwurf
        // -----------
        eingezahlterGesamtbetrag = 0.0;
        while(eingezahlterGesamtbetrag < zuZahlen)
        {
            // ergaentzt zwei die Nachkommastellen
            System.out.printf("Noch zu zahlen: %.2f " ,  (zuZahlen - eingezahlterGesamtbetrag));
            System.out.print("Eingabe (mind. 5Ct, hoechstens 2 Euro): ");
            eingeworfeneMuenze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneMuenze;
        }
        rueckgabebetrag = eingezahlterGesamtbetrag - zuZahlen;
        return rueckgabebetrag;
    }

    // Fahrkarten Ausgabe
    static void fahrkartenAusgeben() {

        // Fahrscheinausgabe
        // -----------------
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
            System.out.print("=");
            warte(250);
        }
        System.out.println("\n\n");
    }

    // Pausiert das Programm
    static void warte(int millisekunden) {
        try {
            Thread.sleep(millisekunden);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    // Rueckgelg Ausgabe
    static void rueckgeldAusgeben(double rueckgabebetrag) {
        final String EURO = "€ ";
        final String CENT = "ct";
        // Rueckgeldberechnung und -Ausgabe
        // -------------------------------

        {
            System.out.printf("Der Rueckgabebetrag in Hoehe von %.2f EURO%n", rueckgabebetrag);
            System.out.println("wird in folgenden Muenzen ausgezahlt:");

            while(rueckgabebetrag >= 2.0) // 2 EURO-Muenzen
            {
                muenzeAusgeben(2.0, EURO);
                rueckgabebetrag -= 2.0;
            }
            while(rueckgabebetrag >= 1.0) // 1 EURO-Muenzen
            {
                muenzeAusgeben(1.0, EURO);
                rueckgabebetrag -= 1.0;
            }
            while(rueckgabebetrag >= 0.5) // 50 CENT-Muenzen
            {
                muenzeAusgeben(0.5, CENT);
                rueckgabebetrag -= 0.5;
            }
            while(rueckgabebetrag >= 0.2) // 20 CENT-Muenzen
            {
                muenzeAusgeben(0.2, CENT);
                rueckgabebetrag -= 0.2;
            }
            while(rueckgabebetrag >= 0.1) // 10 CENT-Muenzen
            {
                muenzeAusgeben(0.1, CENT);
                rueckgabebetrag -= 0.1;
            }
            while(rueckgabebetrag >= 0.05)// 5 CENT-Muenzen
            {
                muenzeAusgeben(0.05, CENT);
                rueckgabebetrag -= 0.05;
            }
        }
    }

    // Muenzen Ausgabe
    static void muenzeAusgeben(double betrag, String einheit) {
        betrag = betrag < 1.0 ? betrag * 100 : betrag;
        System.out.printf("%d %s", (int) betrag, einheit);
    }

    // Abschied
    static void abschied() {
        System.out.println("""
                Vergessen Sie nicht, den Fahrschein
                vor Fahrtantritt entwerten zu lassen!
                Wir wuenschen Ihnen eine gute Fahrt.
                """);
    }

}
