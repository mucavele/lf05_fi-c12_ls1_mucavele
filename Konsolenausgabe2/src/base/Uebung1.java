package base;

public class Uebung1 {

	public static void main(String[] args) {
		// Aufgabe 1
		System.out.println("Aufgabe 1");

		
		System.out.print("Beispielsatz 1");
		System.out.println("Beispielsatz 2");
		
		// Beispielsatz mit Escape symbolen
		System.out.print("Beispielsatz \"1\" \n");
		
		// Beispielsatz mit Verkettung
		System.out.println("Zweite Ausgabe: " + "Beispielsatz 2");
		
		// Das ist ein Kommentar
		
		System.out.println();
		
		// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		
		// Aufgabe 2
		System.out.println("Aufgabe 2");
				
		System.out.println("      *");
		System.out.println("     ***");
		System.out.println("    *****");
		System.out.println("   *******");
		System.out.println("  *********");
		System.out.println(" ***********");
		System.out.println("*************");
		System.out.println("     ***     ");
		System.out.println("     ***     ");
		System.out.println();
		
		// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
				
		// Aufgabe 3
				
		System.out.println("Aufgabe 3");
			
		System.out.printf("Zahl eins: %.2f\n", 22.4234234);
		System.out.printf("Zahl zwei: %.2f\n", 111.2222);
		System.out.printf("Zahl drei: %.2f\n", 4.0);
		System.out.printf("Zahl vier: %.2f\n", 1000000.551);
		System.out.printf("Zahl fünf: %.2f\n", 97.34);
	}

}
