package base;

public class Uebung2 {

	public static void main(String[] args) {
		// Aufgabe 3
		System.out.printf("Fahrenheit%2s|%-3sCelsius%n", "", "");
		System.out.println("------------------------");
		System.out.printf("%+-12d|%10.2f%n", -20, -28.89);
		System.out.printf("%-12d|%10.2f%n", -10, -23.33);
		System.out.printf("%+-12d|%10.2f%n", 0, -17.78);
		System.out.printf("%+-12d|%10.2f%n", 20, -6.67);
		System.out.printf("%+-12d|%10.2f%n", -30, -1.11);
	}
}